﻿using Ionic.Zip;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinSCP;

namespace OsuPlayerReleaseHelper {


    public partial class MainWindow : Window {

        String path = "";
        String releasePath = @"X:\Repositorys\OsuPlayer\OsuPlayer\bin\Release";

        BackgroundWorker bw = new BackgroundWorker();

        public MainWindow() {
            InitializeComponent();

            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);

            SetPath();

            if (File.Exists("server.info"))
                LoadBoxes();
        }

        void SetPath() {
            path = @"C:\Users\Founntain\Desktop\";
            pathLabel.Content = path;
        }

        private void outputBtn_Click(object sender, RoutedEventArgs e) {
            CommonOpenFileDialog od = new CommonOpenFileDialog();

            od.IsFolderPicker = true;

            if (od.ShowDialog() == CommonFileDialogResult.Ok) {
                pathLabel.Content = "Path: " + od.FileName;
                path = od.FileName;
            }
        }

        private void Start_shits_Click(object sender, RoutedEventArgs e) {

            PackRelease();

        }

        void PackRelease() {
            Debug.WriteLine("starting...");

            var filesToZip = new List<String>();

            using (ZipFile zip = new ZipFile("osu!player.zip")) {
                String[] files = Directory.GetFiles(releasePath);

                Debug.WriteLine("start getting files for osu!player.zip");
                foreach (String s in files) {
                    if (s.Contains(".exe")) {
                        if (!s.Contains(".config")) {
                            Debug.WriteLine("adding files: " + s.Split('\\')[s.Split('\\').Length - 1]);
                            filesToZip.Add(s);
                        }
                    }

                    if (s.Contains(".dll")) {
                        if (!s.Contains("Diagnostic") || !s.Contains("Xml")) {
                            Debug.WriteLine("adding files: " + s.Split('\\')[s.Split('\\').Length - 1]);
                            filesToZip.Add(s);
                        }
                    }
                }

                Debug.WriteLine("adding directories for osu!player.zip");
                zip.AddDirectory(releasePath + "\\lang", "lang");

                zip.AddDirectory(releasePath + "\\Resources", "Resources");

                zip.AddFiles(filesToZip, "");

                zip.Save(path + "\\osu!player.zip");
            }

            var zipFiles = new List<String>();



            using (ZipFile zip = new ZipFile("langs.zip")) {
                String[] langFiles = Directory.GetFiles(releasePath + "\\lang");

                Debug.WriteLine("start getting files for langs.zip");
                foreach (string s in langFiles) {
                    Debug.WriteLine("adding files: " + s.Split('\\')[s.Split('\\').Length - 1]);
                    zipFiles.Add(s);
                }

                zip.AddFiles(zipFiles, "");

                Debug.WriteLine("adding directories for langs.zip");
                zip.Save(path + "\\langs.zip");
            }

            Debug.WriteLine("done...");
        }

        private void serverBox_GotFocus(object sender, RoutedEventArgs e) {
            serverBox.Text = "";
        }

        private void usernameBox_GotFocus(object sender, RoutedEventArgs e) {
            usernameBox.Text = "";
        }

        private void pathBox_GotFocus(object sender, RoutedEventArgs e) {
            pathBox.Text = "";
        }

        void SaveBoxes() {
            String[] data = { serverBox.Text, usernameBox.Text, passBox.Password, pathBox.Text };

            File.WriteAllLines("server.info", data);
        }

        void LoadBoxes() {
            String[] data = File.ReadAllLines("server.info");

            serverBox.Text = "";
            usernameBox.Text = "";
            passBox.Password = "";
            pathBox.Text = "";

            serverBox.Text = data[0];
            usernameBox.Text = data[1];
            passBox.Password = data[2];
            pathBox.Text = data[3];
        }

        struct FTPSetting {
            public String Server { get; set; }
            public String Username { get; set; }
            public String Password { get; set; }
            public String Path { get; set; }
            public String Version { get; set; }
        }

        FTPSetting ftpSetting;

        void bw_DoWork(object sender, DoWorkEventArgs e) {
            try {

                String[] data = File.ReadAllLines("server.info");
                String key = File.ReadAllLines("fingerprint.info")[0];

                SessionOptions sessionOptions = new SessionOptions {
                    Protocol = Protocol.Sftp,
                    HostName = data[0],
                    UserName = data[1],
                    Password = data[2],
                    SshHostKeyFingerprint = key
                };

                using (Session session = new Session()) {
                    // Connect
                    session.Open(sessionOptions);

                    session.CreateDirectory("/home/founntain/www/osuplayer/" + ftpSetting.Version+"/");

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();

                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult =
                        session.PutFiles(@"C:\Users\Founntain\Desktop\osu!player.zip", "/home/founntain/www/osuplayer/" + ftpSetting.Version + "/", false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    foreach (TransferEventArgs transfer in transferResult.Transfers) {
                        Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }

                    TransferOperationResult transferResult1;
                    transferResult1 = session.PutFiles(@"C:\Users\Founntain\Desktop\langs.zip", "/home/founntain/www/osuplayer/lang/", false, transferOptions);

                    transferResult1.Check();

                    // Print results

                    foreach (TransferEventArgs transfer in transferResult1.Transfers) {
                        Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }

                    TransferOperationResult transferResult2;
                    transferResult2 = session.PutFiles(@"C:\Users\Founntain\Desktop\version.txt", "/home/founntain/www/osuplayer/", false, transferOptions);

                    transferResult2.Check();

                    // Print results

                    foreach (TransferEventArgs transfer in transferResult2.Transfers) {
                        Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }

                    File.Delete(path + "/osu!player.zip");
                    File.Delete(path + "/langs.zip");
                    File.Delete(path + "/version.txt");
                }

            } catch (Exception ex) {
                MessageBox.Show(ex + "");
            }
        }

        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            throw new NotImplementedException();
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            sLabel.Text = "Done";
        }

        private void start_shits_Upload_Click(object sender, RoutedEventArgs e) {

            if (versionBox.Text.Equals("v")) {

                MessageBox.Show("Version should not be v");
                return;
            }

            sLabel.Text = "working...";

            PackRelease();

            SaveBoxes();

            ftpSetting.Username = usernameBox.Text;
            ftpSetting.Password = passBox.Password;
            ftpSetting.Server = serverBox.Text;
            ftpSetting.Path = pathBox.Text;

            bw.RunWorkerAsync();
        }

        private void versionBox_TextChanged(object sender, TextChangedEventArgs e) {

            try {
                var data = versionBox.Text.Split('.');

                start_shits_Upload.Content = "Release v" + data[0] + "." + data[1] + ".p";

                File.WriteAllText(path + "/version.txt", data[0] + "." + data[1]);
                Debug.WriteLine("VERSION: " + data[0] + "." + data[1]);

                ftpSetting.Version = "v" + data[0] + "." + data[1];
            } catch(Exception ex) {

            }
        }
    }
}
